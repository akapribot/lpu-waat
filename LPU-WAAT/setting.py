#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Store program constants and configuration settings.
Provides methods to load/save settings from/to the file.

Created on Feb 5, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


from ConfigParser import SafeConfigParser
import logging

import logs


class Settings():
    def __init__(self):
        self.APP_NAME = 'LPU WAAT'
        self.APP_VER = '1.1'
        self.APP_REL = '15 March, 2014'
        self.APP_DEV = 'Ashutosh Kumar Singh'
        self.APP_DEVMAIL = 'me@aksingh.net'
        self.APP_DEVSITE = 'http://www.aksingh.net'
        self.APP_SCOPY = '(c) 2014 ' + self.APP_DEV
        self.APP_COPY = 'Copyright ' + self.APP_SCOPY + \
                        ' <' + self.APP_DEVMAIL + '>'
        self.APP_SITE_HOME = 'http://go.aksingh.net/lpu-waat'
        self.APP_SITE_BUGS = 'http://go.aksingh.net/lpu-waat-bugs'
        self.APP_SITE_DLOADS = 'http://go.aksingh.net/lpu-waat-dloads'
        self.APP_SITE_UPDATE = 'http://code.aksingh.net/lpu-waat/overview'

        self.URL_ADDON = self.APP_DEVSITE
        self.URL_CHECK = ['http://www.google.co.in',
                        'http://www.baidu.com',
                        'http://www.wikipedia.org',
                        'http://www.qq.com',
                        'http://www.amazon.com',
                        'http://wordpress.com',
                        'http://www.bing.com',
                        'http://www.ebay.com',
                        'https://twitter.com',
                        'https://www.linkedin.com',
                        'http://www.ask.com',
                        'http://www.microsoft.com',
                        'https://www.paypal.com',
                        'http://www.apple.com',
                        'http://www.craigslist.org']
        self.URL_CHECKANS = ['google.co.in',
                            'baidu.com',
                            'wikipedia.org',
                            'qq.com',
                            'amazon.com',
                            'wordpress.com',
                            'bing.com',
                            'ebay.com',
                            'twitter.com',
                            'linkedin.com',
                            'ask.com',
                            'microsoft.com',
                            'paypal.com',
                            'apple.com',
                            'craigslist.org']
        self.URL_AUTHEN = 'lpuwifi.com'
        self.URL_FINAL = 'lpu'

        self.URL_STATSUBMIT = 'http://stats.aksingh.net/lpuwaat_submit'
        self.URL_REGCHECK = 'http://stats.aksingh.net/lpuwaat_check'
        self.URL_REGSUCCESS = 'http://stats.aksingh.net/lpuwaat_success'

        self.SLEEPTIME_CHECK = 30  # sleeptime before next authen check
        self.SLEEPTIME_AUTHEN = 60  # sleeptime before next try for authen
        self.SLEEPTIME_ERROR = 1  # sleeptime after error

        self.LIMITTIME_CHECK = 30
        self.LIMITTIME_AUTHEN = 60
        self.LIMITTIME_STATS = 10
        self.LIMITTIME_UPDATE = 300

        self.LIMITNO_STATS = 31
        self.LIMITNO_REG = 11

        self.LOG_LEVEL = logging.INFO
        self.LOG_LEVEL_TXT = 'info'

        self.FILE_SETTINGS = 'settings.ini'
        self.FILE_LOG = 'logs_lpu_waat.log'

        self.login_id = ''
        self.login_pass = ''

        self.sleeptime_checker = self.SLEEPTIME_CHECK

        self.is_log_enabled = True
        self.is_checker_stopped = True  # is InternetAccessChecker stopped?

        self.is_app_registered = False
        self.is_kivy_supported = True
        self.is_update_allowed = True
        self.is_addon_allowed = True  # is browsing addon url allowed?
        self.is_stats_share_allowed = True  # is user is okay to share stats?

        self.had_reg_checked = False
        self.had_stats_shared = False
        self.had_update_checked = False

        self.app_serial = ''
        self.app_update_ver = self.APP_VER
        self.stats_authen = 0  # no. of auto-authentications performed

        self.browser_instance = None  # zope.testbrowser.browser.Browser
        self.queue_instance = None  # Queue.Queue for passing status messages
        self.config_instance = SafeConfigParser()

    def load(self):
        self.config_instance.read(self.FILE_SETTINGS)

        logs.debug('Settings: Loading settings')
        self.is_addon_allowed = self.config_instance.getboolean('app', 'addon')
        self.is_kivy_supported = self.config_instance.getboolean('app', 'kivy')
        self.is_update_allowed = self.config_instance.getboolean('app', 'update')
        self.is_app_registered = self.config_instance.getboolean('app', 'reg')
        self.login_id = self.config_instance.get('login', 'user_id')
        self.login_pass = self.config_instance.get('login', 'user_pass')
        self.stats_authen = self.config_instance.getint('stats', 'authen')
        self.is_stats_share_allowed = (
                            self.config_instance.getboolean('stats', 'share'))
        logs.debug('Settings: Loaded settings')

    def save(self):
        # creating sections if already not present
        # (when program runs for first time or settings file get deleted)
        sec1 = self.config_instance.has_section('app')
        sec2 = self.config_instance.has_section('login')
        sec3 = self.config_instance.has_section('stats')

        if not (sec1 and sec2 and sec3):
            logs.debug('Settings: Creating (settings) sections')
            self.config_instance.add_section('app')
            self.config_instance.add_section('login')
            self.config_instance.add_section('stats')
            logs.debug('Settings: Created (settings) sections')

        logs.debug('Settings: Saving settings')
        self.config_instance.set('app', 'addon', str(self.is_addon_allowed))
        self.config_instance.set('app', 'kivy', str(self.is_kivy_supported))
        self.config_instance.set('app', 'update', str(self.is_update_allowed))
        self.config_instance.set('app', 'reg', str(self.is_app_registered))
        self.config_instance.set('login', 'user_id', self.login_id)
        self.config_instance.set('login', 'user_pass', self.login_pass)
        self.config_instance.set('stats', 'authen', str(self.stats_authen))
        self.config_instance.set('stats', 'share',
                                str(self.is_stats_share_allowed))

        with open(self.FILE_SETTINGS, 'wb') as configfile:
            self.config_instance.write(configfile)
            logs.debug('Settings: Saved settings')
