#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Register the application and report statistics.

Created on Feb 21, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import hashlib
import platform
import subprocess

import http_tools
import logs


class StatsException(Exception):
    pass


class Statistics:
    def __init__(self, settings):
        self.settings = settings
        self.browser = self.settings.browser_instance

    def get_linux_serial(self):
        # calc serial using UUID of root partition in Linux

        logs.debug('Statistics: Getting serial on Linux')
        with open('/etc/fstab', 'r') as ffile:
            for line in ffile:
                if line.find('#') != 0:
                    start_ind = line.find('UUID=')
                    if start_ind != -1:
                        end_ind = line.find(' ', start_ind + 5)
                        serial = line[(start_ind + 5):end_ind]
                        break

        logs.debug('Statistics: Got serial on Linux')
        return serial

    def get_win_serial(self):
        # calc serial using SerialID of hard disk in Windows

        logs.debug('Statistics: Getting serial on Windows')
        line = subprocess.check_output(['wmic', 'DISKDRIVE', 'get',
                                        'SerialNumber'])
        start_ind = line.find('\n')
        end_ind = line.find(' ', start_ind + 1)
        serial = line[(start_ind + 1):end_ind]

        logs.debug('Statistics: Got serial on Windows')
        return serial

    def calc_serial(self):
        serial = ''

        logs.debug('Statistics: Calculating app serial')
        try:
            if platform.system() == 'Windows':
                osys = 'windows'
                serial = self.get_win_serial()
            elif platform.system() == 'Linux':
                osys = 'linux'
                serial = self.get_linux_serial()
        except:
            raise StatsException

        digest = hashlib.sha256(osys + '_' + serial).hexdigest()
        logs.debug('Statistics: Calculated app serial')

        return digest

    def calc_app_serial(self):
        logs.debug('Statistics: Getting app serial')
        serial = self.settings.app_serial

        if serial == '':
            serial = self.calc_serial()
            self.settings.app_serial = serial

        logs.debug('Statistics: Got app serial')

    def submit_info(self):
        self.calc_app_serial()

        # submitting the usage statistics
        logs.debug('Statistics: Submitting the usage statistics')
        post_txt = 'serial={}&version={}&stats-authen={}'
        post_ftxt = post_txt.format(self.settings.app_serial,
                                    self.settings.APP_VER,
                                    self.settings.stats_authen)
        http_tools.add_headers(self.browser, self.settings.URL_STATSUBMIT)
        self.browser.post(self.settings.URL_STATSUBMIT, post_ftxt)

        if self.browser.url.find(self.settings.URL_REGSUCCESS) != -1:
            logs.debug('Statistics: Usage submission successful')
            return True
        else:
            logs.warn('Statistics: Usage submission failure')
            return False

    def check_reg(self):
        self.calc_app_serial()

        # checking for registraton
        logs.debug('Statistics: Checking the registration')
        post_txt = 'serial={}'
        post_ftxt = post_txt.format(self.settings.app_serial)
        http_tools.add_headers(self.browser, self.settings.URL_REGCHECK)
        self.browser.post(self.settings.URL_REGCHECK, post_ftxt)
        logs.debug('Statistics: Checked the registration')

        if self.browser.url.find(self.settings.URL_REGSUCCESS) != -1:
            logs.debug('Statistics: App is registered')
            return True
        else:
            logs.warn('Statistics: App is not registered')
            return False

    def register(self):
        rval = self.check_reg()

        if not rval:
            logs.debug('Statistics: Registering the app')
            rval2 = self.submit_info()
            if rval2:
                self.settings.is_app_registered = True
                self.settings.save()
                logs.debug('Statistics: Registered the app')
            else:
                logs.debug('Statistics: Registered not the app')
        else:
            self.settings.is_app_registered = True
            self.settings.save()
