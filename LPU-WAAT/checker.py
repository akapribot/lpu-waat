#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Checks for connectivity/authentication of the Internet.

Created on Feb 5, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import random

import http_tools
import logs


class InternetChecker():
    def __init__(self, settings):
        self.settings = settings
        self.browser = self.settings.browser_instance

    def check_access(self):
        rand_no = random.randint(0, len(self.settings.URL_CHECK) - 1)

        # checking for the Internet's availability
        logs.debug('Checker: Opening the check URL: {}',
                   self.settings.URL_CHECK[rand_no])

        http_tools.add_headers(self.browser, self.settings.URL_CHECK[rand_no])
        self.browser.open(self.settings.URL_CHECK[rand_no])

        logs.debug('Checker: Opened the check URL: {}',
                      self.browser.url)

        # checking if Internet access is available? Internet access is not
        # available if browser is redirected to any of the authentication URLs

        # check url
        yes = self.browser.url.find(self.settings.URL_CHECKANS[rand_no]) != -1
        # authentication url
        no = self.browser.url.find(self.settings.URL_AUTHEN) != -1

        if yes:
            return True
        elif no:
            return False
