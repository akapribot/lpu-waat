#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Checks for updates.

Created on Mar 13, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import logs

from zope.testbrowser.browser import Browser


class Updater():

    def __init__(self, settings):
        self.settings = settings

    def check_app_update(self):
        browser = Browser()
        browser.mech_browser.set_handle_robots(False)

        logs.debug('Updater: Opening the update URL: {}', self.settings.APP_SITE_UPDATE)
        browser.open(self.settings.APP_SITE_UPDATE)
        logs.debug('Updater: Opened the update URL: {}', browser.url)

        logs.debug('Updater: Finding the version from server')
        end_ind = browser.contents.find('(latest)') - 1
        start_ind = browser.contents.find('>', (end_ind - 10), end_ind)
        strver = browser.contents[(start_ind + 1):end_ind].strip()
        logs.debug('Updater: Found the version from server: {}', strver)

        self.settings.app_update_ver = strver

        ver = float(self.settings.app_update_ver)
        cver = float(self.settings.APP_VER)
        if ver > cver:
            logs.debug('Updater: Found new update: Old - {}, New - {}',
                       str(cver), str(ver))
            self.settings.queue_instance.put('UpdateAvailable')
        else:
            logs.debug('Updater: Found no new update')
