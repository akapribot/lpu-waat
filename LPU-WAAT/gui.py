#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------
"""
Tkinter graphical interface of this program.

Created on Feb 10, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


from Tkinter import *
import tkMessageBox
from ttk import *
import webbrowser

import runner


class Application(Frame):
    def start_checker(self):
        settings = self.settings

        settings.login_id = self.f1.userId.entry.get()
        settings.login_pass = self.f1.userPass.entry.get()
        settings.sleeptime_checker = self.f1.hc.check_delay.get()
        settings.save()
        settings.queue_instance.put('Credentials saved!')

        self.f1.userStart.config(state=DISABLED)
        self.f1.userStop.config(state=NORMAL)

        if settings.is_checker_stopped:
            settings.is_checker_stopped = False
            settings.queue_instance.put('Authenticator started!')

            run_instance = runner.Runner(settings)
            run_instance.daemon = True
            run_instance.start()

    def stop_checker(self):
        settings = self.settings

        settings.is_checker_stopped = True
        settings.queue_instance.put('Authenticator stopped!')

        self.f1.userStop.config(state=DISABLED)
        self.f1.userStart.config(state=NORMAL)

    def exit(self):
        self.stop_checker()
        self.master.destroy()

    def show_status(self, settings):
        if not settings.queue_instance.empty():
            st = settings.queue_instance.get()

            if st == 'UpdateAvailable':
                self.show_update(settings)
            else:
                self.status_bar.status.config(text='Status: ' + st)

        self.status_bar.status.after(100, self.show_status, settings)

    def show_update(self, settings):
        titl = 'Update LPU WAAT'
        mess_tmp = '''
        New version is available.
        Current version: {}
        Latest version: {}\n
        Do you want to download?
        '''
        mess = mess_tmp.format(settings.APP_VER, settings.app_update_ver)

        rval = tkMessageBox.askyesno(titl, mess)
        if rval:
            try:
                webbrowser.open(settings.APP_SITE_DLOADS)
            except Exception:
                pass

    def save_settings(self):
        settings = self.settings

        if self.f2.stat_var.get() == 1:
            settings.is_stats_share_allowed = True
        else:
            settings.is_stats_share_allowed = False

        if self.f2.addon_var.get() == 1:
            settings.is_addon_allowed = True
        else:
            settings.is_addon_allowed = False

        if self.f2.upd_var.get() == 1:
            settings.is_update_allowed = True
        else:
            settings.is_update_allowed = False

        settings.save()

    def create_widgets(self, settings):
        self.n = Notebook(self.master)
        self.n.pack(fill='both', expand=True, side='top')

        self.f0 = Frame(self.n)
        self.f0.pack()
        self.n.add(self.f0, text='Welcome')

        self.f0.welcomeMsg = Label(self.f0)
        self.f0.welcomeMsg.pack(side='top', fill='both', expand=True)
        tmp0 = '''
        Bonjour\n\n
        LPU WAAT stands for LPU Wireless Automatic Authentication Tool.\n
        LPU WAAT is the solution to the wireless authentication problem.
        It can automatically authenticate WiFi for you in Lovely
        Professional University.\n
        Using LPU WAAT, you get free of manually authenticating WiFi
        after every fixed interval. Start auto-authenticating LPU Wireless
        using this application.\n
        Just start LPU WAAT, enter your login details and start the Auto
        Authenticator. Switch to Auto-Authenticator tab for this task.\n
        It's simple yet powerful. Don't believe, try it!\n\n
        NOTE: Please use only below given Close button to exit LPU WAAT.
        '''
        self.f0.welcomeMsg.text = tmp0
        self.f0.welcomeMsg.config(text=self.f0.welcomeMsg.text)

        self.f1 = Frame(self.n)
        self.f1.pack()
        self.n.add(self.f1, text='Auto-Authenticator')

        self.f1.userId = Frame(self.f1)
        self.f1.userId.pack(side='top', fill='x')
        self.f1.userId.label = Label(self.f1.userId, text='\nUsername: ')
        self.f1.userId.label.pack(side='top')
        self.f1.userId.text = StringVar()
        self.f1.userId.text.set(settings.login_id)
        self.f1.userId.entry = Entry(self.f1.userId,
                                    textvariable=self.f1.userId.text)
        self.f1.userId.entry.pack(side='top')

        self.f1.userPass = Frame(self.f1)
        self.f1.userPass.pack(side='top', fill='x')
        self.f1.userPass.label = Label(self.f1.userPass, text='\nPassword: ')
        self.f1.userPass.label.pack(side='top')
        self.f1.userPass.text = StringVar()
        self.f1.userPass.text.set(settings.login_pass)
        self.f1.userPass.entry = Entry(self.f1.userPass, show='*',
                                    textvariable=self.f1.userPass.text)
        self.f1.userPass.entry.pack(side='top')

        self.f1.hc = Frame(self.f1)
        self.f1.hc.pack(side='top', fill='x')

        self.f1.hc.hclabel = Label(self.f1.hc,
                       text='\nDelay between checking Authentication status:')
        self.f1.hc.hclabel.pack(side='top')

        self.f1.hc.check_delay = IntVar()
        self.f1.hc.check2 = Radiobutton(self.f1.hc, text='10 seconds',
                                    value=10, variable=self.f1.hc.check_delay)
        self.f1.hc.check3 = Radiobutton(self.f1.hc, text='20 seconds',
                                    value=20, variable=self.f1.hc.check_delay)
        self.f1.hc.check4 = Radiobutton(self.f1.hc, text='30 seconds',
                                    value=30, variable=self.f1.hc.check_delay)
        self.f1.hc.check5 = Radiobutton(self.f1.hc, text='60 seconds',
                                    value=60, variable=self.f1.hc.check_delay)
        self.f1.hc.check2.pack(side='top')
        self.f1.hc.check3.pack(side='top')
        self.f1.hc.check4.pack(side='top')
        self.f1.hc.check5.pack(side='top')
        self.f1.hc.check_delay.set(settings.sleeptime_checker)

        self.f1.hc.hclabel2 = Label(self.f1.hc, text='')
        self.f1.hc.hclabel2.pack(side='top')

        self.f1.userStart = Button(self.f1, text='Start Auto-Authenticator',
                                command=self.start_checker)
        self.f1.userStart.pack(side='top', fill='x')

        self.f1.userStop = Button(self.f1, text='Stop Auto-Authenticator',
                                command=self.stop_checker, state=DISABLED)
        self.f1.userStop.pack(side='top', fill='x')

        tmp_txt0 = '''
        Do you know?
        Your details are automatically saved when you start auto-authenticator.
        '''
        self.f1.hint = Label(self.f1, text=tmp_txt0)
        self.f1.hint.pack(side='left', fill='x')

        self.f5 = Frame(self.n)
        self.f5.pack()
        self.n.add(self.f5, text='Stats Data')

        self.f5.msg1 = Label(self.f5)
        self.f5.msg1.pack(side='top', fill='both', expand=True)
        tmp_txt2 = '''
        Application serial:
        {}\n
        Total authentications:
        {}\n\n
        What's what?\n
        Application serial is the serial of this instance of LPU WAAT. It cannot
        be used to identify your computer or reach/track you in any way.\n
        Total authentications is the number of auto-authentications done by
        LPU WAAT from the first day of its usage.\n\n
        What's shared?\n
        Application serial is only used for registration and if you allow, then
        total authentications is also shared but only on your choice.
        '''
        self.f5.msg1.text = tmp_txt2.format(settings.app_serial,
                                            settings.stats_authen)
        self.f5.msg1.config(text=self.f5.msg1.text)

        self.f2 = Frame(self.n)
        self.f2.pack()
        self.n.add(self.f2, text='Settings')

        tmp_txt00 = '''
        Check for Updates
        LPU WAAT can automatically check for updates from online server
        and notify you about newer updates, whenever available.
        '''
        self.f2.upd_des = Label(self.f2, text=tmp_txt00)
        self.f2.upd_des.pack(side='top', fill='x')

        self.f2.upd_var = IntVar()
        self.f2.upding = Checkbutton(self.f2,
                                 text='auto-Check for Updates (Recommended)',
                                        variable=self.f2.upd_var,
                                        command=self.save_settings)
        self.f2.upding.pack(side='top')
        if settings.is_update_allowed:
            self.f2.upd_var.set(1)
        else:
            self.f2.upd_var.set(0)

        tmp_txt01 = '''\n
        Share Statistics
        You can share the authentication usage statistics of your LPU
        WAAT instance with the developer. It helps him to understand the 
        usage statistics of this program.
        '''
        self.f2.stat_des = Label(self.f2, text=tmp_txt01)
        self.f2.stat_des.pack(side='top', fill='x')

        self.f2.stat_var = IntVar()
        self.f2.stating = Checkbutton(self.f2,
                                      text='Share Statistics (Recommended)',
                                        variable=self.f2.stat_var,
                                        command=self.save_settings)
        self.f2.stating.pack(side='top')
        if settings.is_stats_share_allowed:
            self.f2.stat_var.set(1)
        else:
            self.f2.stat_var.set(0)

        tmp_txt02 = '''\n
        Addon Website
        You can help the developer by allowing this program to open
        his addon website after successful authentication.
        '''
        self.f2.addon_des = Label(self.f2, text=tmp_txt02)
        self.f2.addon_des.pack(side='top', fill='x')

        self.f2.addon_var = IntVar()
        self.f2.addon = Checkbutton(self.f2,
                                    text='Browse Addon Website (Recommended)',
                                        variable=self.f2.addon_var,
                                        command=self.save_settings)
        self.f2.addon.pack(side='top')
        if settings.is_addon_allowed:
            self.f2.addon_var.set(1)
        else:
            self.f2.addon_var.set(0)

        self.f3 = Frame(self.n)
        self.f3.pack()
        self.n.add(self.f3, text='About')

        self.f3.msg1 = Label(self.f3)
        self.f3.msg1.pack(side='top', fill='both', expand=True)
        tmp_txt1 = '''
        LPU WAAT - LPU Wireless Automatic Authentication Tool\n
        {}\n
        Version: {}    Release: {}\n\n
        Homepage: {}\n
        Bugs or Feature requests: {}\n\n\n
        Developer: {}\n
        Developer homepage: {}\n\n
        This program was not possible without tools or people listed in CREDITS.txt,
        distributed with this software.
        '''
        self.f3.msg1.text = tmp_txt1.format(settings.APP_COPY,
                            settings.APP_VER, settings.APP_REL,
                            settings.APP_SITE_HOME,
                            settings.APP_SITE_BUGS, settings.APP_DEV,
                            settings.APP_DEVSITE)
        self.f3.msg1.config(text=self.f3.msg1.text)

        self.f4 = Frame(self.n)
        self.f4.pack()
        self.n.add(self.f4, text='License')

        self.f4.msg1 = Label(self.f4)
        self.f4.msg1.pack(side='top', fill='both', expand=True)
        tmp_txt2 = '''
        LPU WAAT - LPU Wireless Automatic Authentication Tool
        {}\n
        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject
        to the following conditions:

        The above copyright notice and this permission notice shall be
        included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
        CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
        SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        '''
        self.f4.msg1.text = tmp_txt2.format(settings.APP_COPY)
        self.f4.msg1.config(text=self.f4.msg1.text)

        self.status_bar = Frame(self.master)
        self.status_bar.pack(fill='x', side='bottom')

        self.status_bar.status = Label(self.status_bar,
                                        text='Status: Program started!')
        self.status_bar.status.pack(side='left')

        self.status_bar.copyright = Label(self.status_bar,
                                    text=settings.APP_SCOPY)
        self.status_bar.copyright.pack(side='right')

        self.exit_button = Button(self.master, text='Close LPU WAAT',
                                    command=self.exit)
        self.exit_button.pack(side='bottom', fill='x')

    def __init__(self, settings, master=None):
        Frame.__init__(self, master)
        self.settings = settings

        self.pack()
        self.create_widgets(settings)
        self.show_status(settings)

        # registering the program
        runner.Registerer(settings).start()
