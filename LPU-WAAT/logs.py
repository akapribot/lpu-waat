#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Logging utility.

Created on Mar 13, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""

import logging
import time


def format_msg(msg, *args, **kwargs):
    now = time.strftime('%x %X')
    str_replace = ' ' + now + '~'

    start_ind = msg.find(':')
    msg = msg[:start_ind + 1] + str_replace + msg[start_ind + 1:]

    string = msg.format(*args, **kwargs)

    return string


def debug(msg, *args, **kwargs):
    mess = format_msg(msg, *args, **kwargs)
    logging.debug(mess)


def error(msg, *args, **kwargs):
    mess = format_msg(msg, *args, **kwargs)
    logging.error(mess)


def exception(msg, *args, **kwargs):
    mess = format_msg(msg, *args, **kwargs)
    logging.exception(mess)


def warn(msg, *args, **kwargs):
    mess = format_msg(msg, *args, **kwargs)
    logging.warn(mess)


def info(msg, *args, **kwargs):
    mess = format_msg(msg, *args, **kwargs)
    logging.info(mess)
