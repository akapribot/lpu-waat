#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Authenticates the Internet.

Created on Feb 5, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import http_tools
import logs


class InternetAuthenticator():
    def __init__(self, settings):
        self.settings = settings
        self.browser = self.settings.browser_instance

    def browse_addon(self):
        # browse the addon url
        logs.debug('Authenticator: Opening addon page')
        http_tools.add_headers(self.browser, self.settings.URL_ADDON)
        self.browser.open(self.settings.URL_ADDON)
        logs.debug('Authenticator: Opened addon page')

        if self.browser.url.find(self.settings.URL_ADDON):
            logs.debug('Authenticator: Addon successful')
            return True
        else:
            logs.warn('Authenticator: Addon failure')
            return False

    def authenticate(self):
        # checking if authentication page is opened?
        # OR it was only a False Alarm
        try:
            link = self.browser.url
        except Exception:
            link = None

        if link is not None:
            no = self.browser.url.find(self.settings.URL_AUTHEN) != -1

            if not no:
                # browser has not opened any authentication link
                # thus False Alarm and thus returning False for
                # failed authentication because no authentication can be done
                return False
        else:
            # browser has not opened any link, thus False Alarm
            # and thus returning False for failed authentication
            # because no authentication is done
            return False

        # authentication page is already opened in the browser
        logs.debug('Authenticator: Already opened URL: {}', link)

        # trying to authenticate

        # login form
        logs.debug('Authenticator: Getting the form controls')
        for frm in self.browser.mech_browser.forms():
            for ctrl in frm.controls:
                if ctrl.type == 'text':
                    sid = ctrl
                elif ctrl.type == 'password':
                    spass = ctrl
                elif ctrl.type == 'checkbox':
                    scheck = ctrl
            break

        logs.debug('Authenticator: Got the form controls')

        logs.debug('Authenticator: Filling the form details')
        sid.value = self.settings.login_id
        spass.value = self.settings.login_pass
        scheck.value = ['on']
        logs.debug('Authenticator: Filled the form details')

        logs.debug('Authenticator: Submitting the form')
        self.browser.getForm(index=0).submit()
        logs.debug('Authenticator: Submitted the form')

        if self.browser.url.find(self.settings.URL_FINAL) == -1:
            # authentication process is yet not completed

            # first redirect
            logs.debug('Authenticator: Finding first redirect page')
            start_ind1 = self.browser.contents.find('href=\'')
            if start_ind1 != -1:
                end_ind1 = self.browser.contents.find('\';', start_ind1 + 6)
                redirect_page1 = self.browser.contents[(start_ind1 + 6):end_ind1]

                # redirecting
                http_tools.add_headers(self.browser, redirect_page1)
                logs.debug('Authenticator: Opening first redirect page: {}',
                              redirect_page1)
                self.browser.open(redirect_page1)
                logs.debug('Authenticator: Opened first redirect page: {}',
                              self.browser.url)

            # second redirect
            logs.debug('Authenticator: Finding second redirect page')
            start_ind2 = self.browser.contents.find('href=\'')
            if start_ind2 != -1:
                end_ind2 = self.browser.contents.find('\';', start_ind2 + 6)
                redirect_page2 = self.browser.contents[(start_ind2 + 6):end_ind2]

            # authentication link
            logs.debug('Authenticator: Finding authentication link')
            start_inda = self.browser.contents.find('src = "')
            if start_inda != -1:
                end_inda = self.browser.contents.find('";', start_inda + 7)
                authen_page = self.browser.contents[(start_inda + 7):end_inda]

                # authenticating
                http_tools.add_headers(self.browser, authen_page)
                logs.debug('Authenticator: Opening authentication link: {}',
                              authen_page)
                self.browser.open(authen_page)
                logs.debug('Authenticator: Opened authentication link: {}',
                              self.browser.url)

            # second redirect
            if start_ind2 != -1:
                http_tools.add_headers(self.browser, redirect_page2)
                logs.debug('Authenticator: Opening second redirect page: {}',
                              redirect_page2)
                self.browser.open(redirect_page2)
                logs.debug('Authenticator: Opened second redirected page: {}',
                              self.browser.url)

        # checking for successful authentication
        if self.browser.url.find(self.settings.URL_FINAL) != -1:
            logs.debug('Authenticator: Authentication successful')

            # updating the usage statistics
            logs.debug('Authenticator: Updating the authen statistics')
            self.settings.stats_authen += 1
            self.settings.save()
            logs.debug('Authenticator: Updated the authen statistics')

            return True
        else:
            logs.warn('Authenticator: Authentication failure')
            return False
