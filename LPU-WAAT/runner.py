#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Runs and distributes all the tasks in various small threads.

Created on Feb 25, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import sys
from threading import Thread
import time

import authenticator
import checker
import logs
import stats
import updater


class TimeLimitException(Exception):
    pass


def timed_thread(timeout, func):
    """ Run func with the given timeout. If func didn't finish running
        within the timeout, raise TimeLimitExpired
    """
    class TimeLimitedThread(Thread):
        def __init__(self):
            Thread.__init__(self)
            self.result = None

        def run(self):
            self.result = func()

        def _stop(self):
            if self.isAlive():
                Thread._Thread__stop(self)

    it = TimeLimitedThread()
    it.daemon = True
    it.start()
    it.join(timeout)
    if it.isAlive():
        it._stop()
        raise TimeLimitException()
    else:
        return it.result


class Registerer(Thread):
    def __init__(self, settings):
        Thread.__init__(self)
        self.settings = settings
        self.daemon = True

    def run(self):
        self.reg_at_start()

    def reg_at_start(self):
        st = stats.Statistics(self.settings)

        # checking for registration
        if not self.settings.is_app_registered:
            try:
                logs.debug('Registerer: Trying to register')
                timed_thread(self.settings.LIMITTIME_STATS, st.register)
                logs.debug('Registerer: Tried to register')
                self.settings.had_reg_checked = True
            except TimeLimitException:
                logs.debug('Registerer: Time expired for check/register the app')
            except Exception:
                logs.debug('Registerer: Can not check/register the app')

                excinfo = str(sys.exc_info())
                logs.exception('Runner: Exception details: \n{}', excinfo)

                # delay the loop as exception occurred
                # otherwise same error will be continuously get reported
                time.sleep(self.settings.SLEEPTIME_ERROR)


class Runner(Thread):
    def __init__(self, settings):
        Thread.__init__(self)
        self.settings = settings
        self.is_authenticated = False
        self.is_authen_succeeded = False

    def check(self):
        self.is_authenticated = False

        try:
            ic = checker.InternetChecker(self.settings)
            self.is_authenticated = timed_thread(self.settings.LIMITTIME_CHECK,
                                     ic.check_access)

        except TimeLimitException:
            self.settings.queue_instance.put(
                                         'Time expired! Re-checking...')
            logs.debug('Runner: Time expired for checking the Internet')

        except Exception:
            self.settings.queue_instance.put('Error! Re-checking...')
            logs.debug('Runner: Can not check for Internet')

            excinfo = str(sys.exc_info())
            logs.exception('Runner: Exception details: \n{}', excinfo)

            # delay the loop as exception occurred
            # otherwise same error will be continuously get reported
            time.sleep(self.settings.SLEEPTIME_ERROR)

    def authen(self):
        self.is_authen_succeeded = False

        try:
            ia = authenticator.InternetAuthenticator(self.settings)
            self.is_authen_succeeded = timed_thread(
                            self.settings.LIMITTIME_AUTHEN, ia.authenticate)

        except TimeLimitException:
            self.settings.queue_instance.put(
                                         'Time expired! Re-authenticating...')
            logs.debug('Runner: Time expired for authenticating Internet')

        except Exception:
            self.settings.queue_instance.put('Error! Re-authenticating...')
            logs.debug('Runner: Can not authenticate the Internet')

            excinfo = str(sys.exc_info())
            logs.exception('Runner: Exception details: \n{}',excinfo)

            # delay the loop as exception occurred
            # otherwise same error will be continuously get reported
            time.sleep(self.settings.SLEEPTIME_ERROR)

    def update(self):
        try:
            if not self.settings.had_update_checked:
                upd = updater.Updater(self.settings)
                self.is_authen_succeeded = timed_thread(
                        self.settings.LIMITTIME_UPDATE, upd.check_app_update)
                self.settings.had_update_checked = True

        except TimeLimitException:
            logs.debug('Runner: Time expired for checking Update')

        except Exception:
            logs.debug('Runner: Can not check for update')

            excinfo = str(sys.exc_info())
            logs.exception('Runner: Exception details: \n{}', excinfo)

            # delay the loop as exception occurred
            # otherwise same error will be continuously get reported
            time.sleep(self.settings.SLEEPTIME_ERROR)

    def reg(self):
        st = stats.Statistics(self.settings)

        # checking for registration
        if not self.settings.is_app_registered:
            try:
                timed_thread(self.settings.LIMITTIME_STATS, st.register)
                self.settings.had_reg_checked = True
            except TimeLimitException:
                logs.debug('Runner: Time expired for check/register the app')
            except Exception:
                logs.debug('Runner: Can not check/register the app')

                excinfo = str(sys.exc_info())
                logs.exception('Runner: Exception details: \n{}', excinfo)

                # delay the loop as exception occurred
                # otherwise same error will be continuously get reported
                time.sleep(self.settings.SLEEPTIME_ERROR)
        else:
            try:
                chek = self.settings.stats_authen % self.settings.LIMITNO_REG == 1

                if not self.settings.had_reg_checked and chek:
                    timed_thread(self.settings.LIMITTIME_STATS, st.register)
                    self.settings.had_reg_checked = True
            except TimeLimitException:
                logs.debug('Runner: Time expired for check/register the app')
            except Exception:
                logs.debug('Runner: Can not check/register the app')

                excinfo = str(sys.exc_info())
                logs.exception('Runner: Exception details: \n{}', excinfo)

                # delay the loop as exception occurred
                # otherwise same error will be continuously get reported
                time.sleep(self.settings.SLEEPTIME_ERROR)

    def stat(self):
        st = stats.Statistics(self.settings)

        # submitting the statistics
        try:
            if self.settings.is_stats_share_allowed:
                chek = self.settings.stats_authen % self.settings.LIMITNO_STATS == 1
                if not self.settings.had_stats_shared and chek:
                    timed_thread(self.settings.LIMITTIME_STATS, st.submit_info)
                    self.settings.had_stats_shared = True
        except TimeLimitException:
            logs.debug('Runner: Time expired for submitting statistics')
        except Exception:
            logs.debug('Runner: Can not submit statistics')

            excinfo = str(sys.exc_info())
            logs.exception('Runner: Exception details: \n{}', excinfo)

            # delay the loop as exception occurred
            # otherwise same error will be continuously get reported
            time.sleep(self.settings.SLEEPTIME_ERROR)

    def run(self):
        # thread's loop
        while(not self.settings.is_checker_stopped):
            if self.settings.is_checker_stopped:
                break  # end the Runner's thread

            # checking for Internet availability
            logs.debug('Runner: Checking for Internet availability')
            # putting status to 'Checking...' only if not already authenticated
            if not self.is_authenticated:
                self.settings.queue_instance.put('Checking...')

            self.check()

            if self.settings.is_checker_stopped:
                break  # end the Runner's thread

            # authenticating the Internet is not already authenticated
            if not self.is_authenticated:
                logs.warn('Runner: Internet not authenticated')
                self.settings.queue_instance.put(
                                             'Not authenticated! Trying...')
                # authenticating
                self.authen()

                if self.settings.is_checker_stopped:
                    break  # end the Runner's thread

                if not self.is_authen_succeeded:
                    logs.info('Runner: Authentication failed')
                    self.settings.queue_instance.put('Authentication failed!')
                else:
                    logs.info('Runner: Authentication succeeded')
                    self.settings.queue_instance.put('Authenticated!')
                    # sleep after the Internet got authenticated
                    time.sleep(self.settings.SLEEPTIME_AUTHEN)
            else:
                logs.debug('Runner: Authentication already succeeded')
                self.settings.queue_instance.put('Authenticated!')

            if self.is_authenticated:
                # registration and statistics
                logs.debug('Runner: Checking registration')
                self.reg()
                logs.debug('Runner: Checked registration')

                if self.settings.is_checker_stopped:
                    break  # end the Runner's thread

                logs.debug('Runner: Submitting statistics')
                self.stat()
                logs.debug('Runner: Submitted statistics')

                if self.settings.is_checker_stopped:
                    break  # end the Runner's thread

                # check for updates
                logs.debug('Runner: Checking for update')
                self.update()
                logs.debug('Runner: Checked for update')

                if self.settings.is_checker_stopped:
                    break  # end the Runner's thread

                # sleep if Internet is already authenticated
                time.sleep(self.settings.sleeptime_checker)

        # thread's while loop got over
