#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------
"""
Starts the GUI of the program.

Created on Feb 5, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""

import ConfigParser
import Queue
from Tkinter import PhotoImage
import logging
import platform

import gui  # Tkinter GUI
import gui2 # Kivy GUI
import logs
import setting  # program's constants and setting
import stats
from zope.testbrowser.browser import Browser


def main():
    # loading setting
    settings = setting.Settings()

    try:
        settings.load()
    except ConfigParser.Error:
        # error means setting file not present or deleted, probably
        settings.save()

    # getting the serial
    stats.Statistics(settings).calc_app_serial()

    logging.basicConfig(filename=settings.FILE_LOG, level=settings.LOG_LEVEL,
                        format='[%(levelname)s] %(message)s')
    logs.info('WAAT: LPU WAAT started')

    settings.browser_instance = Browser()
    settings.queue_instance = Queue.Queue()

    # do not respect robots.txt setting to avoid this program getting blocked
    settings.browser_instance.mech_browser.set_handle_robots(False)

    # showing GUI
    if settings.is_kivy_supported:
        try:
            app = gui2.Application(settings=settings, kv_file='gui2.kv',
                                   use_kivy_settings=False)
            logs.debug('WAAT: Starting the (Kivy) GUI')
            app.run()
        except:
            logs.warn('WAAT: Kivy is not supported')
            settings.is_kivy_supported = False
            settings.save()
    else:
        logs.debug('WAAT: Starting the (Tkinter) GUI')
        app = gui.Application(settings)
        if platform.system() == 'Windows':
            app.master.iconbitmap(default='data/icons/waat.ico')
        app.master.title(settings.APP_NAME + ' v' + settings.APP_VER)
        app.master.minsize(500, 300)
        app.mainloop()


if __name__ == '__main__':
    main()
