#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Tools for working with HTTP.

Created on Mar 2, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import platform

import logs

from zope.testbrowser.browser import Browser


def add_headers(browser, url):
    """
    Adds required headers to the browser.
    """
    # getting the host url
    start_ind = url.find('://')
    end_ind = url.find('/', start_ind + 3)
    host_url = url[(start_ind + 3):end_ind]

    logs.debug('HTTP_Tools: Adding headers for URL: {}', url)

    # adding headers
    browser.addHeader('Host', host_url)

    if platform.system() == 'Windows':
        browser.addHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; \
        x64; rv:27.0) Gecko/20100101 Firefox/27.0')
    elif platform.system() == 'Linux':
        browser.addHeader('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; \
        Linux x86_64; rv:26.0) Gecko/20100101 Firefox/27.0')

    browser.addHeader('Accept', 'text/html,application/xhtml+xml,\
    application/xml;q=0.9,*/*;q=0.8')
    browser.addHeader('Accept-Language', 'en-US,en;q=0.5')
    browser.addHeader('Connection', 'keep-alive')

    logs.debug('HTTP_Tools: Added headers for URL: {}', url)
