#!/usr/bin/python
#-----------------------------------------------------------------------------
# Copyright (c) 2014, Ashutosh Kumar Singh <me@aksingh.net>
#
# Distributed under the terms of the MIT License.
#
# The full license is in the file COPYING.txt, distributed with this software.
#-----------------------------------------------------------------------------


"""
Kivy graphical interface of this program.

Created on Feb 16, 2014

@author: Ashutosh Kumar Singh <me@aksingh.net>
"""


import kivy
from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup

import webbrowser

import runner

kivy.require('1.8.0')


class Controller(BoxLayout):

    status_msg = ObjectProperty()

    smhome_text = ObjectProperty()
    smswitch_text = ObjectProperty()
    smswitch_pvalue = ObjectProperty()
    smenjoy_text = ObjectProperty()

    input_username = ObjectProperty()
    input_userpass = ObjectProperty()

    start_authen = ObjectProperty()
    stop_authen = ObjectProperty()

    smfaq_text = ObjectProperty()
    smabout_text = ObjectProperty()
    smstatsdata_text = ObjectProperty()

    smsettingsstat_text = ObjectProperty()
    smsettingsaddon_text = ObjectProperty()
    smsettingsupd_text = ObjectProperty()

    smsettingsstat_check = ObjectProperty()
    smsettingsaddon_check = ObjectProperty()
    smsettingsupd_check = ObjectProperty()

    checktime_slider = ObjectProperty()

    update_label = None
    update_popup = None

    def __init__(self, settings, **kwargs):
        self.settings = settings
        super(BoxLayout, self).__init__(**kwargs)

    def update_settings(self, dt):
        settings = self.settings
        settings.sleeptime_checker = self.checktime_slider.value
        self.smswitch_pvalue.text = '[size=18]Present value = {}[/size]'.format(
                                            int(settings.sleeptime_checker))

    def open_link(self, instance, value):
        try:
            if value == 'dhp':
                webbrowser.open(self.settings.APP_DEVSITE)
            elif value == 'ahp':
                webbrowser.open(self.settings.APP_SITE_HOME)
            elif value == 'abf':
                webbrowser.open(self.settings.APP_SITE_BUGS)
            elif value == 'upd':
                webbrowser.open(self.settings.APP_SITE_DLOADS)
            elif value == 'man-upd':
                runner.Runner().update()
        except Exception:
            pass

    def show_status(self, dt):
        if not self.settings.queue_instance.empty():
            st = self.settings.queue_instance.get()

            if st == 'UpdateAvailable':
                upd_tmp = '''
                [size=18]
                A new version of LPU WAAT is available.
                Current version: {}
                Latest version: {}\n
                [ref=upd][color=1560BD]Download the new version of LPU WAAT[/color][/ref]
                (click above to open the download page)
                [/size]
                '''
                self.update_label.text = upd_tmp.format(self.settings.APP_VER,
                                                   self.settings.app_update_ver)
                self.update_popup.open()
            else:
                self.status_msg.text = '[b]Status:[/b] ' + st
                if st == 'Authenticated!':
                    tmp_sdata = '''
                    [size=55][b]Stats Data[/b][/size]
                    [size=18]
                    [b]Application serial:[/b]
                    {}\n
                    [b]Total authentications:[/b] {}\n\n
                    [b]What's what?[/b]
                    Application serial is the serial of this instance of LPU WAAT. It cannot
                    be used to identify your computer or reach/track you in any way.\n
                    Total authentications is the number of auto-authentications done by
                    LPU WAAT from the first day of its usage.\n
                    [b]What's shared?[/b]
                    Application serial is only used for registration and if you allow, then
                    total authentications is also shared but only on your choice.
                    [/size]
                    '''
                    self.smstatsdata_text.text = tmp_sdata.format(
                                                   self.settings.app_serial,
                                                   self.settings.stats_authen)

    def save_details(self):
        settings = self.settings

        settings.login_id = self.input_username.text
        settings.login_pass = self.input_userpass.text
        settings.save()

        settings.queue_instance.put('Credentials saved!')

    def switchon_authen(self):
        settings = self.settings

        if settings.is_checker_stopped:
            settings.is_checker_stopped = False
            settings.queue_instance.put('Authenticator started!')

            run_instance = runner.Runner(settings)
            run_instance.daemon = True
            run_instance.start()

        self.start_authen.disabled = True
        self.stop_authen.disabled = False

    def switchoff_authen(self):
        settings = self.settings

        settings.is_checker_stopped = True
        settings.queue_instance.put('Authenticator stopped!')

        self.start_authen.disabled = False
        self.stop_authen.disabled = True

    def bye_bye(self):
        App.get_running_app().stop()

    def on_stop(self):
        """Event handler which is called when the app's window is going
        to be closed.
        """
        self.switchoff_authen()

    def check_stats(self, checkbox, value):
        settings = self.settings

        if value:
            settings.is_stats_share_allowed = True
        else:
            settings.is_stats_share_allowed = False

        settings.save()

    def check_addon(self, checkbox, value):
        settings = self.settings

        if value:
            settings.is_addon_allowed = True
        else:
            settings.is_addon_allowed = False

        settings.save()

    def check_upd(self, checkbox, value):
        settings = self.settings

        if value:
            settings.is_update_allowed = True
        else:
            settings.is_update_allowed = False

        settings.save()


class Application(App):

    settings = None

    def __init__(self, settings, **kwargs):
        self.settings = settings
        super(Application, self).__init__(**kwargs)

    def get_application_config(self):
        return super(Application, self).get_application_config(
            '%(appdir)s/settings.ini')

    def build_config(self, config):
        Config.set('kivy', 'window_icon', 'data/icons/waat.png')
        Config.set('kivy', 'desktop', 1)
        Config.set('kivy', 'exit_on_escape', 0)
        Config.set('kivy', 'keyboard_mode', 'system')
        Config.set('kivy', 'log_level', self.settings.LOG_LEVEL_TXT)
        Config.set('kivy', 'log_dir', self.directory + '/logs')
        Config.set('kivy', 'log_name', 'lpu_waat_%y-%m-%d_%_.log')
        Config.set('graphics', 'width', 800)
        Config.set('graphics', 'height', 600)
        Config.set('graphics', 'fullscreen', 0)
        Config.set('graphics', 'resizable', 0)
        Config.write()

    def build(self):
        self.title = self.settings.APP_NAME + ' v' + self.settings.APP_VER
        self.icon = 'data/icons/waat.png'

        # configuring the application
        self.use_kivy_settings = False

        # root widget
        self.ctrl = Controller(self.settings)

        return self.ctrl

    def on_start(self):
        # defining the UI elements
        tmp0 = '''
        [size=55][b]Bonjour[/b][/size]\n\n
        [size=18]
        LPU WAAT stands for LPU Wireless Automatic Authentication Tool.\n
        LPU WAAT is the solution to the wireless authentication problem.
        It can automatically authenticate WiFi for you in Lovely
        Professional University.\n
        Using LPU WAAT, you get free of manually authenticating WiFi
        after every fixed interval. Start auto-authenticating LPU Wireless
        using this application.\n
        Just complete 4 steps to enjoy browsing un-interrupted Internet.\n
        It's simple yet powerful. Don't believe, try it!
        [/size]
        '''
        self.ctrl.smhome_text.text = tmp0

        self.ctrl.smswitch_text.text = '''
        [size=18]
        Auto-Authenticator is the tool which is responsible for doing
        automatic authentications. You must switch it on to get an
        un-interrupted Internet.
        [/size]
        '''

        self.ctrl.smenjoy_text.text = '''
        [size=55][b]Sit back & Enjoy[/b][/size]\n\n\n
        [size=18]
        That was all needed to be done.\n
        I'm not kidding. :-)\n\n
        Sit back, relax and enjoy browsing the Internet.\n
        No more authentication failure while chatting with your friend
        or downloading your favorite movie or TV show.\n
        It's simply great, isn't it?
        [/size]
        '''

        self.ctrl.smsettingsupd_text.text = '''
        [size=18]\n\n
        [b]Check for Updates[/b]
        This program will automatically check for updates from online server and notify
        you about newer updates, whenever available.
        [/size]
        '''

        self.ctrl.smsettingsstat_text.text = '''
        [size=18]\n\n
        [b]Share Statistics[/b]
        You can share the total authentications of your LPU WAAT instance with the      
        developer. It helps him to understand the usage statistics of this program.
        [/size]
        '''

        self.ctrl.smsettingsaddon_text.text = '''
        [size=18]\n\n
        [b]Addon Website[/b]
        You can help the developer by allowing this program to open his addon website
        after successful authentication. He will be thankful to you for the same.
        [/size]
        '''

        tmp_sdata = '''
        [size=55][b]Stats Data[/b][/size]
        [size=18]
        [b]Application serial:[/b]
        {}\n
        [b]Total authentications:[/b]
        {}\n\n
        [b]What's what?[/b]
        Application serial is the serial of this instance of LPU WAAT. It cannot be
        used to identify your computer or reach/track you in any way.\n
        Total authentications is the number of auto-authentications done by this
        instance of LPU WAAT from the first day of its usage.\n
        [b]What's shared?[/b]
        Application serial is only used for registration and if you allow, then total
        authentications is also shared but only on your choice (see settings).
        [/size]
        '''
        self.ctrl.smstatsdata_text.text = tmp_sdata.format(
                                           self.settings.app_serial,
                                           self.settings.stats_authen)

        self.ctrl.smfaq_text.text = '''
        [size=55][b]FAQ[/b][/size]\n
        [size=18]
        [b]Q: What is LPU WAAT?[/b]
        [b]A:[/b] LPU WAAT is an automatic authenticator for the WiFi network at Lovely Professional
            University. You no longer need to enter user name and password after every fixed
            interval and Internet will never go down because of expired authentication.\n
        [b]Q: Why registration is necessary?[/b]
        [b]A:[/b] LPU WAAT registers itself online for the same reason other programs like 
            your anti-virus or office do: registration helps developer to calculate installations
            which pushes him to continue working to improve it.\n
        [b]Q: What data is required for registration?[/b]
        [b]A:[/b] LPU WAAT only requires a self-generated unique key of this instance for the
            registration. This key is unique for a system. This program takes no personal
            information for registration and your personal data remains on your system only.\n
        [b]Q: How to get help?[/b]
        [b]A:[/b] Please check the 'about' page for links to request features or report bugs.
        [/size]
        '''

        self.ctrl.smabout_text.text = '''
        [size=55][b]About[/b][/size]\n
        [size=20][b] LPU WAAT[/b] - LPU Wireless Automatic Authentication Tool[/size]
        [size=18]
        {}\n
        [b]Version:[/b] {}    [b]Release:[/b] {}\n
        [b]Homepage:[/b] [ref=ahp][color=4B0082]{}[/color][/ref]
        [b]Bugs or Feature requests:[/b] [ref=abf][color=4B0082]{}[/color][/ref]\n
        This program is distributed under the terms of MIT license.
        The full license is in the file COPYING.txt, distributed with this software.\n\n
        [b]Developer:[/b] {}
        [b]Developer homepage:[/b] [ref=dhp][color=4B0082]{}[/color][/ref]\n
        This program was not possible without tools or people listed in CREDITS.txt,
        distributed with this software.
        [/size]
        '''.format(self.settings.APP_COPY, self.settings.APP_VER,
            self.settings.APP_REL, self.settings.APP_SITE_HOME,
            self.settings.APP_SITE_BUGS, self.settings.APP_DEV,
            self.settings.APP_DEVSITE)

        self.ctrl.update_label = Label(markup=True)
        upd_tmp = '''
        [size=18]
        A new version of LPU WAAT is available.
        Current version: {}
        Latest version: {}\n
        [ref=update][color=1560BD]Download the new version of LPU WAAT[/color][/ref]
        (click above to open the download page)
        [/size]
        '''
        self.ctrl.update_label.text = upd_tmp.format(self.settings.APP_VER,
                                                   self.settings.app_update_ver)
        self.ctrl.update_popup = Popup(title='LPU WAAT\'s Update Available',
                                       content=self.ctrl.update_label,
                                       size_hint=(.6, .5))

        # binding checkboxes
        self.ctrl.smsettingsstat_check.bind(active=self.ctrl.check_stats)
        self.ctrl.smsettingsaddon_check.bind(active=self.ctrl.check_addon)
        self.ctrl.smsettingsupd_check.bind(active=self.ctrl.check_upd)

        # binding labels
        self.ctrl.smabout_text.bind(on_ref_press=self.ctrl.open_link)
        self.ctrl.update_label.bind(on_ref_press=self.ctrl.open_link)

        # update status regularly
        Clock.schedule_interval(self.ctrl.show_status, 1)
        Clock.schedule_interval(self.ctrl.update_settings, 1)

        # registering the program
        runner.Registerer(self.settings).start()
