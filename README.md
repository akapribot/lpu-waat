# LPU WAAT


### LPU Wireless Automatic Authentication Tool

LPU WAAT can automatically authenticate the LPU Wireless, the Wi-Fi service provided in Lovely Professional University. It's an automatic authentication system, much like the automatic transmission system found in cars.

You no longer need to manually do the authentication chore yourself to access Internet in the college campus. LPU WAAT can complete the whole authentication process for you. You can save yourself from wasting time in doing manual authentications.

You can sit back, relax and enjoy Internet browsing without worrying of the connection being disconnected due to expired authentication with LPU WAAT. That's all.

> It's simple, yet powerful.


#### Important Note

Please note that LPU WAAT automatically registers itself at [AKSingh's Stats](http://stats.aksingh.net).  
Please check PRIVACY_POLICY.txt (distributed with this software) for any issues regarding privacy or the data used for registration.


### 4 Steps to get un-Interrupted Internet

1. Open LPU WAAT
2. Type-in Credentials (can be saved)
3. Start auto-Authenticator
4. Enjoy un-interrupted Internet


### Advantages

1. Un-interrupted Internet
2. Un-interrupted Downloads
3. Un-interrupted Chats


### Features

1. Easy-to-use user interface
2. Automatic and regular checking for Internet access
3. Automatic authentication
4. Save credentials (login details)
5. Configuration options



## Downloads

You can download the software's binaries for Windows and Linux from [LPU WAAT's Downloads](http://go.aksingh.net/lpu-waat-dloads).



## Requirements

* Operating System:
    1. Windows - Windows XP or above
    2. Linux - Any (such as Debian, Ubuntu, Fedora, Linux Mint, etc.)
  
* Platform: All (32-bit and 64-bit)

* Disk space: 30-50 MB

* RAM: 512 MB or more



## Installation Instructions

1. Download LPU WAAT for your operating system (Windows/Linux)
2. Unzip the downloaded zip archive
3. Save the extracted folder at any place in your computer

You can **launch LPU WAAT** by opening **LPU WAAT\lpu_waat** (exe or linux executable based on your operating system).



## Versions

### 1.1 (latest)

1. Update notifier
2. Better logging
3. Performance improvements
4. Better graphical interface


#### 1.0

1. Two graphical interfaces
    1. Kivy (multi-touch elegant interface)
    2. tk/Tkinter (fallback-supported simple interface)
2. Automatic checking and authentication



## Source code

You can get the software's source code from [LPU WAAT's Source](http://go.aksingh.net/lpuwaat-src).



## Bugs / Feature Requests

Got a problem, error, or bug in the software?  
Or looking for a new feature that's not already there in LPU WAAT?

Let me know about it and I will try to solve/add it in future release(s).  
Post bugs or feature requests at [LPU WAAT's Bugs/Requests](http://go.aksingh.net/lpuwaat-bugs).



## Developer

**Ashutosh Kumar Singh** | [www.aksingh.net](http://www.aksingh.net) | me@aksingh.net



## Credits

LPU WAAT was not possible without these excellent tools or people:

1. [Zope Test Browser](https://pypi.python.org/pypi/zope.testbrowser)
2. [Kivy](http://kivy.org)
3. [Eldorado Bundle Mini Icons](http://www.icojam.com/blog/?p=626)
4. Testing / Feedback (official):
    * Abhishek Kulkarni
    * Aman Sonewane
    * Dhruv Gupta
    * Gaurav Gupta



## License

LPU WAAT - LPU Wireless Automatic Authentication Tool  
Copyright (c)2014 Ashutosh Kumar Singh <me@aksingh.net\>

LPU WAAT is distributed under the terms of MIT License.  
The full license is in the file COPYING.txt, distributed with this software.



## Screenshots

### Kivy - Advanced multi-touch Graphical interface

![Home (Kivy GUI)](http://i1345.photobucket.com/albums/p675/akapribot/LPU%20WAAT/BonjourKivy_zpsc689287e.png "Home (Kivy GUI\)")

![Auto-Authenticator (Kivy GUI)](http://i1345.photobucket.com/albums/p675/akapribot/LPU%20WAAT/Auto-AuthenticatorKivy_zps72686af4.png "Auto-Authenticator (Kivy GUI\)")




### tk/Tkinter - Simple fallback-supported Graphical interface

![Home (tk GUI)](http://i1345.photobucket.com/albums/p675/akapribot/LPU%20WAAT/Bonjourtk_zpsdd3c96bc.png "Home (tk GUI\)")

![Auto-Authenticator (tk GUI)](http://i1345.photobucket.com/albums/p675/akapribot/LPU%20WAAT/Auto-Authenticatortk_zps8a745dca.png "Auto-Authenticator (tk GUI\)")

